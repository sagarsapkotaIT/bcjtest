package com.sagar.icon;

import com.iconloop.score.token.irc2.IRC2Basic;
import score.Address;
import score.Context;
import score.VarDB;
import score.annotation.External;

import java.math.BigInteger;


public abstract class IRC2MintableBurnable extends IRC2Basic {

    private final VarDB<Address> minter = Context.newVarDB("minter", Address.class);

    public IRC2MintableBurnable(String _name, String _symbol, int _decimals) {
        super(_name, _symbol, _decimals);

        if (minter.get() == null) {
            minter.set(Context.getOwner());
        }
    }

    /**
     * Destroys `_amount` tokens from the caller.
     */
    @External
    public void burn(BigInteger _amount) {
        _burn(Context.getCaller(), _amount);
    }


    /**
     * Creates _amount number of tokens, and assigns to caller.
     * Increases the balance of that account and the total supply.
     */
    @External
    public void mint(BigInteger _amount) {
        // simple access control - only the minter can mint new token
        Context.require(Context.getCaller().equals(minter.get()));
        _mint(Context.getCaller(), _amount);
    }

    /**
     * Creates _amount number of tokens, and assigns to _account.
     * Increases the balance of that account and the total supply.
     */
    @External
    public void mintTo(Address _account, BigInteger _amount) {
        // simple access control - only the minter can mint new token
        Context.require(Context.getCaller().equals(minter.get()));
        _mint(_account, _amount);
    }

    @External
    public void setMinter(Address _minter) {
        // simple access control - only the contract owner can set new minter
        Context.require(Context.getCaller().equals(Context.getOwner()));
        minter.set(_minter);
    }

}
